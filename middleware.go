package main

import (
	"strings"
	"image"
	"errors"
	"fmt"
	"math"
	"github.com/anthonynsimon/bild/transform"
	"image/draw"
	"github.com/anthonynsimon/bild/blend"
	"bitbucket.org/pixelsband/tilemill/common"
)

const PostProcessFiltersAllowed = "resize,grayscale,overlay"

func filterFactory(filterNames string) (filters []map[string]baseFilter, err error) {

	lost := []string{}

	for _, n := range strings.Split(PostProcessFiltersAllowed, ",") {

		name := strings.TrimSpace(n)

		if name == "resize" {
			filters = append(filters, map[string]baseFilter{name:resizeFilter})
		} else if name == "grayscale" {
			filters = append(filters, map[string]baseFilter{name:grayscaleFilter})
		} else if name == "overlay" {
			filters = append(filters, map[string]baseFilter{name:overlayFilter})
		} else {
			lost = append(lost, name)
		}
	}

	if len(lost) > 0 {
		err = errors.New(fmt.Sprintf("Filter %s not found.", strings.Join(lost, ",")))
	}

	return
}

type baseFilter func(tile common.Tile, src image.RGBA) (img *image.RGBA, err error)

func resizeFilter(tile common.Tile, src image.RGBA) (img *image.RGBA, err error) {

	return &src, nil
}

func grayscaleFilter(tile common.Tile, src image.RGBA) (img *image.RGBA, err error) {

	img = &src

	/*	if rand.Intn(5) == 1 {

			usr, err := imgio.Open("./data/fixtures/343.jpeg")

			if err != nil {
				return img, err
			}

			// no need to resize, since overlay in real size? if more than tile size? retina?
			tmp := transform.Resize(clone.AsRGBA(usr), 128, 128, transform.NearestNeighbor)
			tile.Overlay = tmp.Pix
		}


		if tile.Overlay != nil {
			// result := effect.Grayscale(img)
		}
	*/
	return img, err
}

func overlayFilter(tile common.Tile, src image.RGBA) (img *image.RGBA, err error) {

	if tile.Overlay != nil  {

		// overlay := &image.RGBA{tile.Overlay, 4 * 128, image.Rect(0,0, 256, 256)}
		overlay := image.NewRGBA(image.Rect(0,0, 128, 128))

		avatarsOnSide := int(math.Pow(2, float64(tile.Z)))
		fragSize := 128 / avatarsOnSide

		// img = clone.AsRGBA(&src)

		for x := 0; x < avatarsOnSide; x++ {
			for y := 0; y < avatarsOnSide; y++ {

				tmp := &image.RGBA{tile.Overlay, 4 * 128, image.Rect(0,0, 128, 128)}
				frag := transform.Resize(tmp, fragSize, fragSize, transform.NearestNeighbor)
				x1,y1,x2,y2 := x * fragSize, y * fragSize, x * fragSize + fragSize, y * fragSize + fragSize
				draw.Draw(overlay, image.Rect(x1,y1,x2,y2), frag, frag.Bounds().Min, draw.Src)

				// img = blend.Opacity(&src, frag, 10)
			}
		}

		// side := int(math.Sqrt(float64(len(tile.Background))) / 2)
		img = blend.Opacity(&src, overlay, .5)

		return img, nil
	}

	return &src, nil
}

func FilterPipe(tile common.Tile, filterNames string) (img *image.RGBA, err error) {

	/*	filters, err := filterFactory(filterNames)
		skipped := []string{}

		if err != nil {
			log.Fatal(err)
		}*/

	size := int(math.Sqrt(float64(len(tile.Background))) / 2)
	tmp := &image.RGBA{tile.Background, 4 * size, image.Rect(0,0, size, size)}
	img = transform.Resize(tmp, 128, 128, transform.NearestNeighbor)

	if len(tile.Overlay) > 0 {

		overlay := &image.RGBA{Pix: tile.Overlay, Stride: 4 * 128, Rect: image.Rect(0,0, 128, 128)}
		// draw.Draw(img, img.Bounds(), overlay, image.ZP, draw.Over)

		maxZoom := 6
		img = blend.Opacity(img, overlay, float64(maxZoom - tile.Z) / 10)
		// todo transparency
	}


	/*
	next := clone.AsRGBA(img)

	for _, filter := range filters {

		// todo: replace this hell with iterations with fn as a method of struct
		for name, fn := range filter {

			img, err = fn(tile, *next)

			if err != nil {
				skipped = append(skipped, name)
				continue
			}

			b := img.Bounds()
			fmt.Println(b.Size())
			next = img  // clone.AsRGBA(img)
		}
	}

	if len(skipped) > 0 {
		msg := "Filters %s caused errors was skipped from processing. Check log records for more details."
		err = errors.New(fmt.Sprintf(msg, strings.Join(skipped, ",")))
	}
*/
	return
}