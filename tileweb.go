package main

import (
	"strconv"
	"math"
	"fmt"
	"bytes"
	"github.com/valyala/fasthttp"
	"bitbucket.org/pixelsband/tilemill/common"
	"image/png"
	"strings"
)

func main() {

	config, err := common.Configs().Get("test")

	if err != nil {
		panic(err)
	}

	Run(config)
}

func Run(config common.Config) {

	store := common.TileStorage{config}

	// expires := time.Now().Add(time.Hour * 24 * 2).Format(time.RFC3339)

	yourIPRequestHandler := func(ctx *fasthttp.RequestCtx) {

		Route(ctx, &store)

		x, y, z, err := parseArgs(ctx)

		fmt.Println(x, y, z)

		if err != nil {
			ctx.Error("Not correct coordinates", 404)
			return
		}
	}

	// Start fasthttp server returning your ip in response headers.
	fasthttp.ListenAndServe(":8080", yourIPRequestHandler)
}

func Route(ctx *fasthttp.RequestCtx, store *common.TileStorage) (err error) {

	ArgToInt := func(name string) (int, error) {
		n, err := strconv.Atoi(string(ctx.QueryArgs().Peek(name)))
		return int(math.Abs(float64(n))), err
	}

	ArgToStr := func(name string) (string, bool) {
		str := string(ctx.QueryArgs().Peek(name))
		return strings.TrimSpace(str), len(str) > 0
	}

	route, ok := ArgToStr("r")

	if !ok {
		ctx.NotFound()
	} else if route == "tile" {

		coords := []int{}

		for _, axis := range [3]string{"x", "y", "z"} {

			coord, err := ArgToInt(axis)

			if err != nil {
				ctx.Error("Invalid tile coordinates by axis " + axis, 404)
				return err
			}

			coords = append(coords, coord)
		}

		fmt.Println(coords)

		serveTileMiddleware(coords[0], coords[1], coords[2], store, ctx)

	} else if route == "info" {

	}

	return
}

func serveTileMiddleware(x,y,z int, store *common.TileStorage, ctx *fasthttp.RequestCtx) {

	tile, err := store.Find(x, y, z)

	if err != nil {
		fmt.Println(err)
		ctx.Error("Tile not found.", 404)
		ctx.NotFound()
		return

	} else {

		img, err := FilterPipe(tile, "resize")

		if err == nil && len(img.Pix) > 0 {

			mime := "image/png"
			ctx.Response.Header.SetContentType(mime)
			ctx.Response.Header.SetContentLength(len(img.Pix))
			// ctx.Response.Header.SetLastModified(tile.Modified)
			// ctx.Response.Header.Set("Cache-Control", "max-age=86400")
			// ctx.Response.Header.Set("Expires", expires)
			ctx.Response.Header.Set("Pragma", "public")

			var b bytes.Buffer

			if err := png.Encode(&b, img); err != nil {
				panic(err)
			}

			ctx.Response.AppendBody(b.Bytes())

		} else {
			fmt.Println(err)
			ctx.Error("Error during applying filters.", 404)
			ctx.NotFound()
		}

		// fasthttp.ReleaseByteBuffer(b)
	}
}


func parseArgs(ctx *fasthttp.RequestCtx) (x, y, z int, err error) {

	ArgToInt := func(name string) (int, error) {
		n, err := strconv.Atoi(string(ctx.QueryArgs().Peek(name)))
		return int(math.Abs(float64(n))), err
	}

	x, err = ArgToInt("x")
	y, err = ArgToInt("y")
	z, err = ArgToInt("z")

	return x, y, z, err
}
